const path = require('path');
const Hapi = require('hapi');

const config = require('./config');
const plugins = require('./plugins');
const server = Hapi.Server({
    port: 2222,
    routes: {
        cors: {
            origin: ['*'],
            credentials: true,
            headers: ["Accept", "Authorization", "Content-Type", "If-None-Match", "Accept-language"]
        },
        files: {
            relativeTo: path.join(__dirname, 'public')
        },
    }
});

const bootstrap = Promise.all(plugins.map(plugin => server.register(plugin(config))));

bootstrap
    .then(() => {
        console.log('Bootstrapping finished...');
        server.start();
    })
    .catch((err) => {
        console.log(err)
    });
