const path = require('path');

const config = {
    api: {
        legacy: {
            port: '8080',
            protocol: 'http',
            host: 'localhost',
            passThrough: true,
        },
    },
    project: {
        root: __dirname
    },
    wurst: {
        log: true,
        ignore: ['**/*.handler.js', '**/*.off.js'],
        cwd: path.resolve(__dirname, 'routes'),
    },
    hemera: {
        nats: 'nats://localhost:4222',
    },
    graphql: {
        path: '/graphql',
        route: {
            cors: true,
        },
        graphqlOptions: {
            schema: require('./graphql/schema'),
        },
    },
    graphiql: {
        path: '/graphiql',
        graphiqlOptions: {
            endpointURL: '/graphql'
        }
    },
    swagger: {
        swaggerUI: true,
        info: {
            title: 'API Documentation',
            version: '1.0.0',
        },
        grouping: 'tags',
        tags: [
            { name: 'api', description: "API" },
            { name: 'system', description: "System API" },
            { name: 'proxy', description: "Proxy for Legacy API" },
            { name: 'static', description: "Serving static files" },
        ],
    },
    cors: {
        origins: ['*'],
    },
    cron: {
        jobs: require('./cron'),
    },
};

module.exports = config;
