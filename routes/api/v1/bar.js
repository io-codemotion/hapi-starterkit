module.exports = [
    {
        method: 'GET',
        path: '/bar',
        config: {
            handler: () => 'foo',
            description: 'Bar URI',
            notes: 'Get "bar" string from server',
            tags: ["api", 'bar'],
            validate: {},
        },
    },
];