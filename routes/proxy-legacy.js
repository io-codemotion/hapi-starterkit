const config = require('../config');

const proxy = (methods, defaults) => ({
    method: methods,
    path: '/{param*}',
    config: {
        ...defaults,
        validate: {},
        tags: ["api", "proxy"],
        description: 'API Proxy',
        notes: 'Proxy for old API version',
        handler: (r, h) => h.proxy(config.api.legacy),
    },
});

module.exports = [
    proxy(['GET'], {}),
    proxy(['POST', 'PUT', 'DELETE', 'PATCH'], { payload: { output: 'stream', parse: false } }),
];
