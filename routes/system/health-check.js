module.exports = [
    {
        method: 'GET',
        path: '/health-check',
        config: {
            handler: () => 'ok',
            description: 'Health Check',
            notes: 'Get API Status',
            tags: ['api', 'system'],
            validate: {},
        },
    },
];