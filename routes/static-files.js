const config = require('../config');

module.exports = [
    {
        method: 'GET',
        path: '/public/{param*}',
        config: {
            validate: {},
            tags: ["api", "static"],
            description: 'Static files',
            notes: 'Serving static file for client',
            handler: {
                directory: {
                    path: '.',
                    redirectToSlash: true,
                    index: true,
                }
            },
        },
    }
];