const healthcheck = {
    name: 'healthcheck',
    time: '*/60 * * * * *',
    timezone: 'Europe/Kiev',
    request: {
        method: 'get',
        url: '/system/health-check'
    },
    onComplete: () => console.log('System status: ok'),
};

module.exports = healthcheck;
