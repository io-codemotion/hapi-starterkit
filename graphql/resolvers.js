const resolvers = {
    Query: {
        health: () => 'ok'
    }
};

module.exports = resolvers;
