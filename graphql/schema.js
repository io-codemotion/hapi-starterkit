const tools = require('graphql-tools');

const types = require('./types');
const resolvers = require('./resolvers');

const schema = tools.makeExecutableSchema({
    typeDefs: types,
    resolvers: resolvers,
});

module.exports = schema;
