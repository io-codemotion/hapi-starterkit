const HapiInert = require('inert');
const HapiVisionn = require('vision');
const HapiSwagger = require('hapi-swagger');

const swagger = function(options) {
    return [
        HapiInert,
        HapiVisionn,
        {
            plugin: HapiSwagger,
            options: options.swagger
        }
    ];
};

module.exports = swagger;
