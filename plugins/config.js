const config = function(options) {
    return {
        name: 'conf',
        register: (server) => server.bind({ config: options }),
    }
};

module.exports = config;
