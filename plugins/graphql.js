const HapiGraphql = require('apollo-server-hapi');

const graphql = function(options) {
    return {
        plugin: HapiGraphql.graphqlHapi,
        options: options.graphql,
    }
};

module.exports = graphql;
