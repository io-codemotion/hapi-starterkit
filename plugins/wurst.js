const HapiWurst = require('wurst');

const wurst = function(options) {
    return {
        plugin: HapiWurst,
        options: options.wurst
    }
};

module.exports = wurst;
