const HapiHemera = require('hapi-hemera');
const HemeraJoi = require('hapi-hemera');

const hemera = function(options) {
    return {
        plugin: HapiHemera,
        options: {
            hemera:{
                name: 'test',
                logLevel: 'info'
            },
            nats: options.hemera.nats,
        }
    }
};

module.exports = hemera;
