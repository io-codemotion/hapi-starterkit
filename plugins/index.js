const plugins = [
    require('./nes'),
    require('./cron'),
    require('./proxy'),
    require('./wurst'),
    require('./config'),
    require('./hemera'),
    require('./swagger'),
    require('./graphql'),
    require('./graphiql'),
    require('./static-files'),
];

module.exports = plugins;

