const HapiGraphql = require('apollo-server-hapi');

const graphiql = function(options) {
    return {
        plugin: HapiGraphql.graphiqlHapi,
        options: options.graphiql,
    }
};

module.exports = graphiql;
