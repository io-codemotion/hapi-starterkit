const HapiCron = require('hapi-cron');

const cron = function(options) {
    return {
        plugin: HapiCron,
        options: options.cron,
    }
};

module.exports = cron;
